﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebApiThrottle;

namespace CodeKarma.UI.App_Start
{
   public static class WebApiConfig
   {
      public static void Register(HttpConfiguration config)
      {
         //// Web API configuration and services
         //config.Formatters[0] =
         //   new NewtonsoftJsonFormatter(new JsonSerializerSettings()
         //   {
         //      ContractResolver = new CamelCasePropertyNamesContractResolver()
         //   });

          config.MessageHandlers.Add(new ThrottlingHandler()
          {
              Policy = new ThrottlePolicy(perSecond: 1, perMinute: 2, perHour: 20)
              {
                  IpThrottling = true,
                  EndpointThrottling = true
              },
              Repository = new CacheRepository()
          });


            // Web API routes
          config.MapHttpAttributeRoutes();

          config.Routes.MapHttpRoute(
              name: "DefaultApi",
              routeTemplate: "api/{controller}/{id}",
              defaults: new {id = RouteParameter.Optional}
              );
      }
   }
}