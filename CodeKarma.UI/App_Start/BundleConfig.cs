﻿// // •—————————————————————————————————————————————————————————————————————•
// // | Copyright © 2015 CodeKarma Technologies. All Rights Reserved.       |
// // •—————————————————————————————————————————————————————————————————————•

using System.Collections.Generic;
using System.IO;
using System.Web.Optimization;

namespace CodeKarma.UI.App_Start
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add((new ScriptBundle("~/Assets/Unify/js/default"){Orderer = new NonOrderingBundleOrderer()}).Include(
                "~/Assets/plugins/jquery/jquery.min.js",
                "~/Assets/plugins/jquery/jquery-migrate.min.js",
                "~/Assets/plugins/bootstrap/js/bootstrap.min.js"
                ));

            bundles.Add(new ScriptBundle("~/Assets/Unify/js/plugins").Include(
                "~/Assets/plugins/back-to-top.js",
                "~/Assets/plugins/smoothScroll.js",
                "~/Assets/plugins/jquery.easing.min.js",
                "~/Assets/plugins/pace/pace.min.js",
                "~/Assets/plugins/jquery.parallax.js",
                "~/Assets/plugins/owl-carousel/owl.carousel.js",
                "~/Assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js",
                "~/Assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js",
                "~/Assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js",
                "~/Assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"
                ));

            bundles.Add(new ScriptBundle("~/Assets/Unify/js/custom").Include(
                "~/Assets/js/custom.js",
                "~/Assets/js/one.app.js",
                "~/Assets/js/forms/login.js",
                "~/Assets/js/forms/contact.js",
                "~/Assets/js/pace-loader.js",
                "~/Assets/js/revolution-slider.js"
                ));

            bundles.Add(new StyleBundle("~/Assets/Unify/css/default").Include(
                //Css global compulsory
                "~/Assets/plugins/bootstrap/css/bootstrap.min.css",
                "~/Assets/css/one.style.css",
                //CSS Header and Footer
                "~/Assets/css/headers/header-default.css",
                "~/Assets/css/footers/footer-v7.css",
                //CSS Implementing Plugins
                "~/Assets/plugins/animate.css",
                "~/Assets/plugins/line-icons/line-icons.css",
                "~/Assets/plugins/font-awesome/css/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Assets/Unify/css/custom").Include(
                //CSS Theme
                "~/Assets/css/theme-colors/orange.css",
                //CSS Customization
                "~/Assets/css/custom.css"));
            
        }

        public class NonOrderingBundleOrderer : IBundleOrderer
        {
            public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
            {
                return files;
            }
        }
    }
}