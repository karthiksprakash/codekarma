﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using CodeKarma.Services.Zoho;
using Piranha.Extend;

namespace CodeKarma.UI
{
   public class CodeKarmaModule : IModule
   {
      public void Init()
      {
         Mapper.CreateMap<Models.ContactUsModel, Entities.ContactRequest>()
            .ForMember(m => m.Id, o => o.Ignore())
            .ForMember(m => m.Created, o => o.Ignore())
            .ForMember(m => m.Updated, o => o.Ignore());

         Mapper.AssertConfigurationIsValid();

         SetAutofacWebApi();
      }

      /// <summary>
      /// Setup Autofac IOC
      /// </summary>
      private static void SetAutofacWebApi()
      {
         var builder = new ContainerBuilder();

         //Register Zoho CRM service
         builder.Register(
            c =>
               new CrmService(Piranha.App.Logger, Properties.Settings.Default.ZohoCrmBaseUrl,
                  Piranha.Utils.FromConfig<string>("CrmAuthKey")))
            .As<ICrmService>().InstancePerLifetimeScope();


         // Get your HttpConfiguration.
         var config = GlobalConfiguration.Configuration;

         // Register your Web API controllers.
         builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
         //config.Filters.Add(new ArrayInputAttribute("id"));

         // OPTIONAL: Register the Autofac filter provider.
         //builder.RegisterWebApiFilterProvider(config);

         // Set the dependency resolver to be Autofac.
         var container = builder.Build();
         config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
      }
      
   }
}