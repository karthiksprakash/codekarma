﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Piranha.Hooks;

namespace CodeKarma.UI.Controllers
{
    /// <summary>
    /// Default controller for displaying a post archive.
    /// </summary>
    public class ArchiveController : Piranha.AspNet.Mvc.ArchiveController
    {
        /// <summary>
        /// Gets the view for the currently requested archive.
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Index()
        {
            var model = GetModel<Models.ArchiveModel>();

            return View(model.View, model);
        }
    }
}