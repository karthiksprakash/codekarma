﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Piranha.AspNet.Cache;

namespace CodeKarma.UI.Controllers
{
    public class HomeController : Piranha.AspNet.Mvc.PageController
    {
        // GET: Home
        public ActionResult Index()
        {
            var model = GetModel();

            if (!HttpContext.IsCached(model.Id.ToString(), model.GetLastModified()))
                return View(model);
            return null;
        }
    }
}