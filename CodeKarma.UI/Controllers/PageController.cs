﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Piranha.AspNet.Cache;

namespace CodeKarma.UI.Controllers
{
    /// <summary>
    /// Default controller for displaying a page.
    /// </summary>
    public class PageController : Piranha.AspNet.Mvc.PageController
    {
        /// <summary>
        /// Gets the view for the current page.
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Index()
        {
            var model = GetModel();

            if (!HttpContext.IsCached(model.Id.ToString(), model.GetLastModified()))
                return View(model);
            return null;
        }
    }
}