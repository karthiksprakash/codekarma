﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using CodeKarma.Services.Zoho;
using CodeKarma.UI.Models;
using Piranha.Client.Models;
using RazorEngine.Templating;

namespace CodeKarma.UI.Controllers
{
    public class ContactUsController : ApiController
    {
        private readonly ICrmService _crmService;

        public ContactUsController(ICrmService crmService)
        {
            _crmService = crmService;
        }

        [HttpGet]
        [Route("api/Ping")]
        public IHttpActionResult GetTestString()
        {
            return GetResponseContent<string>("Pong", HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("api/ContactUs")]
        public async virtual Task<IHttpActionResult> SendEmail([FromBody] ContactUsModel contactUsModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Get the user IP address
                    contactUsModel.IpAddress = Piranha.AspNet.Utils.GetWebApiUserIp(Request);

                    //Post to CRM
                    await _crmService.AddNewContactRequestAsLeadAsync("CodeKarmaUI",
                      AutoMapper.Mapper.Map<Entities.ContactRequest>(contactUsModel)).ContinueWith((antecedent) => 
                      {
                          if (antecedent.IsFaulted)
                          {
                              Piranha.App.Logger.Log(Piranha.Log.LogLevel.ERROR, "Error adding new contact request to CRM.", antecedent.Exception);
                          }
                          else
                          {
                              ResponseModel<string> crmResponse = antecedent.Result;
                              Piranha.App.Logger.Log(crmResponse.IsSuccess ? Piranha.Log.LogLevel.INFO : Piranha.Log.LogLevel.ERROR, crmResponse.Message);
                          }
                      });

                    //Prepare email
                    var reader = new AppSettingsReader();
                    string recipientEmail = (string)reader.GetValue("ContactUsRecipient", typeof(string));

                    var emailTemplateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "EmailTemplates");
                    var emailTemplatePath = Path.Combine(emailTemplateFolderPath, "ContactUs.cshtml");
                    IRazorEngineService razorEngineService = RazorEngineService.Create();
                    Piranha.Mail.Message message = new Piranha.Mail.Message
                    {
                        Body = razorEngineService.RunCompile(File.ReadAllText(emailTemplatePath), "ContactUsEmail", typeof(ContactUsModel), contactUsModel),// $"Email: {contactUsModel.Email}\n\nMessage:\n{contactUsModel.Message}",
                        Subject = $"New 'ContactUs' request from {contactUsModel.Name}"
                    };

                    //Send email
                    Piranha.App.Mail.Send(message, new[] { new Piranha.Mail.Address("Support", recipientEmail) });

                    return GetResponseContent<string>("Your request has been registered. We'll get back to you as soon as possible. ", HttpStatusCode.Created);
                }
                catch (Exception e)
                {
                    Piranha.App.Logger.Log(Piranha.Log.LogLevel.ERROR, "Error processing ContactUs email.", e);
                }
                
            }
            return GetResponseContent<string>("Invalid request.", HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Contructs a standard response model to be returned as IHttpActionResult
        /// </summary>
        protected virtual IHttpActionResult GetResponseContent<T>(object data, HttpStatusCode httpStatusCode = HttpStatusCode.OK, string message = null)
        {
            HttpResponseMessage response = new HttpResponseMessage(httpStatusCode);

            ResponseModel<T> responseModel = new ResponseModel<T>()
            {
                StatusCode = (int)httpStatusCode,
                Message = message,
                Data = (T)data
            };

            response.Content = new ObjectContent<ResponseModel<T>>(responseModel, new Piranha.AspNet.Infrastructure.JsonNetMediaFormatter());

            return ResponseMessage(response);
        }
    }
}
