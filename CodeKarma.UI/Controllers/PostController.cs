﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Piranha.AspNet.Cache;

namespace CodeKarma.UI.Controllers
{
    /// <summary>
    /// Default controller for displaying a post.
    /// </summary>
    public class PostController : Piranha.AspNet.Mvc.PostController
    {
        /// <summary>
        /// Gets the view for the current post.
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Index()
        {
            var model = GetModel().WithComments();

            if (!HttpContext.IsCached(model.Id.ToString(), model.GetLastModified()))
                return View(model.View, model);
            return null;
        }
    }
}