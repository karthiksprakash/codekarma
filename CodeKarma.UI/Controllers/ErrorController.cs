﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodeKarma.UI.Controllers
{
    /// <summary>
    /// Controller for handling application errors.
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Gets the default error view.
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets the error view for a 404.
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult NotFound()
        {
            return View();
        }
    }
}