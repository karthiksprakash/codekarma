namespace Piranha.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProperties : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PiranhaProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        Value = c.String(),
                        CLRType = c.String(nullable: false, maxLength: 512),
                        IsCollection = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PiranhaPages", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PiranhaProperties", "ParentId", "dbo.PiranhaPages");
            DropIndex("dbo.PiranhaProperties", new[] { "ParentId" });
            DropTable("dbo.PiranhaProperties");
        }
    }
}
