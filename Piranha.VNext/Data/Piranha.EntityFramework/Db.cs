﻿/*
 * Copyright (c) 2014 Håkan Edling
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 * 
 * http://github.com/piranhacms/piranha.vnext
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Threading;

namespace Piranha.EntityFramework
{
	/// <summary>
	/// The internal db context.
	/// </summary>
	public class Db : BaseContext<Db>
	{
		#region Db sets
		/// <summary>
		/// Gets/sets the alias set.
		/// </summary>
		public DbSet<Entities.Alias> Aliases { get; set; }

		/// <summary>
		/// Gets/sets the author set.
		/// </summary>
		public DbSet<Entities.Author> Authors { get; set; }

		/// <summary>
		/// Gets/sets the category set.
		/// </summary>
		public DbSet<Entities.Category> Categories { get; set; }

		/// <summary>
		/// Gets/sets the comment set.
		/// </summary>
		public DbSet<Entities.Comment> Comments { get; set; }

		/// <summary>
		/// Gets/sets the block set.
		/// </summary>
		public DbSet<Entities.Block> Blocks { get; set; }

		/// <summary>
		/// Gets/sets the media set.
		/// </summary>
		public DbSet<Entities.Media> Media { get; set; }

		/// <summary>
		/// Gets/sets the page set.
		/// </summary>
		public DbSet<Entities.Page> Pages { get; set; }

		/// <summary>
		/// Gets/sets the page type set.
		/// </summary>
		public DbSet<Entities.PageType> PageTypes { get; set; }

        /// <summary>
        /// Gets/sets the properties set.
        /// </summary>
        public DbSet<Entities.Property> Properties { get; set; }

		/// <summary>
		/// Gets/sets the page type property set.
		/// </summary>
		public DbSet<Entities.PageTypeProperty> PageTypeProperties { get; set; }

		/// <summary>
		/// Gets/sets the page type region set.
		/// </summary>
		public DbSet<Entities.PageTypeRegion> PageTypeRegions { get; set; }

		/// <summary>
		/// Gets/sets the param set.
		/// </summary>
		public DbSet<Entities.Param> Params { get; set; }

		/// <summary>
		/// Gets/sets the post set.
		/// </summary>
		public DbSet<Entities.Post> Posts { get; set; }

		/// <summary>
		/// Gets/sets the post type set.
		/// </summary>
		public DbSet<Entities.PostType> PostTypes { get; set; }

		/// <summary>
		/// Gets/sets the ratings set.
		/// </summary>
		public DbSet<Entities.Rating> Ratings { get; set; }
		#endregion

		/// <summary>
		/// Default constructor.
		/// </summary>
		public Db() : base("piranha", new MigrateDatabaseToLatestVersion<Db, Migrations.Configuration>()) {}

		/// <summary>
		/// Configures the data model.
		/// </summary>
		/// <param name="modelBuilder">The current model builder</param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			// Alias
			modelBuilder.Entity<Entities.Alias>().ToTable("PiranhaAliases");
			modelBuilder.Entity<Entities.Alias>().Property(a => a.NewUrl).HasMaxLength(255).IsRequired();
			modelBuilder.Entity<Entities.Alias>().Property(a => a.OldUrl).HasMaxLength(255).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));

			// Author
			modelBuilder.Entity<Entities.Author>().ToTable("PiranhaAuthors");
			modelBuilder.Entity<Entities.Author>().Property(a => a.Name).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.Author>().Property(a => a.Email).HasMaxLength(128);
			modelBuilder.Entity<Entities.Author>().Property(a => a.Description).HasMaxLength(512);

			// Block
			modelBuilder.Entity<Entities.Block>().ToTable("PiranhaBlocks");
			modelBuilder.Entity<Entities.Block>().Property(b => b.Name).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.Block>().Property(b => b.Description).HasMaxLength(255);
			modelBuilder.Entity<Entities.Block>().Property(b => b.Slug).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));
	
			// Category
			modelBuilder.Entity<Entities.Category>().ToTable("PiranhaCategories");
			modelBuilder.Entity<Entities.Category>().Property(c => c.Title).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.Category>().Property(c => c.Slug).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));

			// Comment
			modelBuilder.Entity<Entities.Comment>().ToTable("PiranhaComments");
			modelBuilder.Entity<Entities.Comment>().Property(c => c.UserId).HasMaxLength(128);
			modelBuilder.Entity<Entities.Comment>().Property(c => c.Author).HasMaxLength(128);
			modelBuilder.Entity<Entities.Comment>().Property(c => c.Email).HasMaxLength(128);
			modelBuilder.Entity<Entities.Comment>().Property(c => c.IP).HasMaxLength(16);
			modelBuilder.Entity<Entities.Comment>().Property(c => c.UserAgent).HasMaxLength(128);
			modelBuilder.Entity<Entities.Comment>().Property(c => c.WebSite).HasMaxLength(128);
			modelBuilder.Entity<Entities.Comment>().Property(c => c.SessionID).HasMaxLength(64);

			// Media
			modelBuilder.Entity<Entities.Media>().ToTable("PiranhaMedia");
			modelBuilder.Entity<Entities.Media>().Property(m => m.Name).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.Media>().Property(m => m.Slug).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));
				
			// Page
			modelBuilder.Entity<Entities.Page>().ToTable("PiranhaPages");
			modelBuilder.Entity<Entities.Page>().Property(p => p.Title).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.Page>().Property(p => p.NavigationTitle).HasMaxLength(128);
			modelBuilder.Entity<Entities.Page>().Property(p => p.Keywords).HasMaxLength(128);
			modelBuilder.Entity<Entities.Page>().Property(p => p.Description).HasMaxLength(255);
			modelBuilder.Entity<Entities.Page>().Property(p => p.Route).HasMaxLength(255);
			modelBuilder.Entity<Entities.Page>().Property(p => p.View).HasMaxLength(255);
			modelBuilder.Entity<Entities.Page>().Property(p => p.Slug).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));
            modelBuilder.Entity<Entities.Page>().HasMany(t => t.Properties).WithRequired().HasForeignKey(p => p.ParentId).WillCascadeOnDelete(true);

			// PageType
			modelBuilder.Entity<Entities.PageType>().ToTable("PiranhaPageTypes");
			modelBuilder.Entity<Entities.PageType>().Property(t => t.Name).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.PageType>().Property(t => t.Description).HasMaxLength(255);
			modelBuilder.Entity<Entities.PageType>().Property(t => t.Route).HasMaxLength(255);
			modelBuilder.Entity<Entities.PageType>().Property(t => t.View).HasMaxLength(255);
			modelBuilder.Entity<Entities.PageType>().Property(t => t.Slug).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));
			modelBuilder.Entity<Entities.PageType>().HasMany(t => t.Properties).WithRequired().HasForeignKey(p => p.TypeId).WillCascadeOnDelete(true);
			modelBuilder.Entity<Entities.PageType>().HasMany(t => t.Regions).WithRequired().HasForeignKey(r => r.TypeId).WillCascadeOnDelete(true);

			// PageTypeProperty
			modelBuilder.Entity<Entities.PageTypeProperty>().ToTable("PiranhaPageTypeProperties");
			modelBuilder.Entity<Entities.PageTypeProperty>().Property(p => p.Name).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.PageTypeProperty>().Property(p => p.CLRType).HasMaxLength(512).IsRequired();
			modelBuilder.Entity<Entities.PageTypeProperty>().Property(p => p.TypeId)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_InternalId") {
					IsUnique = true,
					Order = 1
				}));
			modelBuilder.Entity<Entities.PageTypeProperty>().Property(r => r.InternalId).HasMaxLength(32).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_InternalId") {
					Order = 2
				}));				

			// PageTypeRegion
			modelBuilder.Entity<Entities.PageTypeRegion>().ToTable("PiranhaPageTypeRegions");
			modelBuilder.Entity<Entities.PageTypeRegion>().Property(r => r.Name).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.PageTypeRegion>().Property(r => r.CLRType).HasMaxLength(512).IsRequired();
			modelBuilder.Entity<Entities.PageTypeRegion>().Property(r => r.TypeId)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_InternalId") {
					IsUnique = true,
					Order = 1
				}));
			modelBuilder.Entity<Entities.PageTypeRegion>().Property(r => r.InternalId).HasMaxLength(32).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_InternalId") {
					Order = 2
				}));				

			// Param
			modelBuilder.Entity<Entities.Param>().ToTable("PiranhaParams");
			modelBuilder.Entity<Entities.Param>().Property(p => p.Name).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));

			// Post
			modelBuilder.Entity<Entities.Post>().ToTable("PiranhaPosts");
			modelBuilder.Entity<Entities.Post>().Property(p => p.Title).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.Post>().Property(p => p.Keywords).HasMaxLength(128);
			modelBuilder.Entity<Entities.Post>().Property(p => p.Description).HasMaxLength(255);
			modelBuilder.Entity<Entities.Post>().Property(p => p.Route).HasMaxLength(255);
			modelBuilder.Entity<Entities.Post>().Property(p => p.View).HasMaxLength(255);
			modelBuilder.Entity<Entities.Post>().Property(p => p.Excerpt).HasMaxLength(512);
			modelBuilder.Entity<Entities.Post>().Property(p => p.TypeId)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_Slug") {
					Order = 1,
					IsUnique = true
				}));
			modelBuilder.Entity<Entities.Post>().Property(p => p.Slug).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_Slug") {
					Order = 2,
					IsUnique = true
				}));
			modelBuilder.Entity<Entities.Post>().Ignore(p => p.CommentCount);
			modelBuilder.Entity<Entities.Post>().HasMany(p => p.Attachments).WithMany().Map(m => m.ToTable("PiranhaPostMedia"));
			modelBuilder.Entity<Entities.Post>().HasMany(p => p.Categories).WithMany().Map(m => m.ToTable("PiranhaPostCategories"));

			// PostType
			modelBuilder.Entity<Entities.PostType>().ToTable("PiranhaPostTypes");
			modelBuilder.Entity<Entities.PostType>().Property(t => t.Name).HasMaxLength(128).IsRequired();
			modelBuilder.Entity<Entities.PostType>().Property(t => t.Description).HasMaxLength(255);
			modelBuilder.Entity<Entities.PostType>().Property(t => t.Route).HasMaxLength(255);
			modelBuilder.Entity<Entities.PostType>().Property(t => t.View).HasMaxLength(255);
			modelBuilder.Entity<Entities.PostType>().Property(p => p.ArchiveTitle).HasMaxLength(128);
			modelBuilder.Entity<Entities.PostType>().Property(p => p.MetaKeywords).HasMaxLength(128);
			modelBuilder.Entity<Entities.PostType>().Property(p => p.MetaDescription).HasMaxLength(255);
			modelBuilder.Entity<Entities.PostType>().Property(p => p.ArchiveRoute).HasMaxLength(255);
			modelBuilder.Entity<Entities.PostType>().Property(p => p.ArchiveView).HasMaxLength(255);
			modelBuilder.Entity<Entities.PostType>().Property(p => p.CommentRoute).HasMaxLength(255);
			modelBuilder.Entity<Entities.PostType>().Property(t => t.Slug).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {
					IsUnique = true
				}));

			// Ratings
			modelBuilder.Entity<Entities.Rating>().ToTable("PiranhaRatings");
			modelBuilder.Entity<Entities.Rating>().Property(r => r.UserId).HasMaxLength(128).IsRequired()
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_UserRating") {
					IsUnique = true,
					Order = 1
				}));
			modelBuilder.Entity<Entities.Rating>().Property(r => r.ModelId)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_UserRating") { 
					Order = 2
				}));
			modelBuilder.Entity<Entities.Rating>().Property(r => r.Type)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_UserRating") {
					Order = 3
				}));

            //Properties
		    modelBuilder.Entity<Entities.Property>().ToTable("PiranhaProperties");
		    modelBuilder.Entity<Entities.Property>().Property(p => p.Name).HasMaxLength(128).IsRequired();
            modelBuilder.Entity<Entities.Property>().Property(p => p.CLRType).HasMaxLength(512).IsRequired();
            //modelBuilder.Entity<Entities.Property>().Property(r => r.InternalId).HasMaxLength(32).IsRequired()
            //    .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute()));				

			base.OnModelCreating(modelBuilder);
		}
	}
}