﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Piranha.AspNet.Infrastructure;

namespace Piranha.AspNet.Extensions
{
    public static class ControllerExtensions
    {
        public static JsonNetResult JsonNet(object data, JsonRequestBehavior jsonRequestBehaviour = JsonRequestBehavior.DenyGet, string contentType = "", Encoding encoding = null,
            Formatting formatting = Formatting.Indented)
        {
            return new JsonNetResult()
            {
                Data = data,
                //SerializerSettings = settings??
                //    new Newtonsoft.Json.JsonSerializerSettings()
                //    {
                //        ContractResolver = new CamelCasePropertyNamesContractResolver()
                //    }
                //,
                ContentType = contentType,
                ContentEncoding = encoding,
                Formatting = formatting,
                JsonRequestBehavior = jsonRequestBehaviour
            };
        }
    }
}
