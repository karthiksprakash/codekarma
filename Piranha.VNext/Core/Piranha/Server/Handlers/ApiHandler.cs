﻿/*
 * Copyright (c) 2016 Karthik Sudandraprakash
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 * 
 * 
 */

using System;
using System.Linq;

namespace Piranha.Server.Handlers
{
	/// <summary>
	/// Handler for routing requests for WebApi.
	/// </summary>
	public class ApiHandler : IHandler
	{
		/// <summary>
		/// Tries to handle an incoming request.
		/// </summary>
		/// <param name="api">The current api</param>
		/// <param name="request">The incoming route request</param>
		/// <returns>The result</returns>
		public IResponse Handle(Api api, IRequest request) {
			var now = DateTime.Now;
			var route = "";

		   if ((request.Segments.Length > 0) && (request.Segments[0].Equals("api", StringComparison.OrdinalIgnoreCase)))
		   {
            route = "api";

            // Append extra url segments
            for (var n = 1; n < request.Segments.Length; n++)
            {
               route += "/" + request.Segments[n];
            }

            // Set current
            App.Env.SetCurrent(new Client.Models.Content()
            {
               Type = Client.Models.ContentType.Api
            });

            var response = request.RewriteResponse();

            response.Route = route;
            response.Params = request.Params;

            return response;
         }
		   return null;
         
      }
	}
}
