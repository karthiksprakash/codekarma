﻿/*
 * Copyright (c) 2014-2015 Håkan Edling
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 * 
 * http://github.com/piranhacms/piranha.vnext
 * 
 */

using System;

namespace Piranha
{
	/// <summary>
	/// The main data api.
	/// </summary>
	public sealed class Api : IDisposable
	{
		#region Members
		/// <summary>
		/// The private db context.
		/// </summary>
		internal readonly Data.IDbSession DbSession;

		/// <summary>
		/// If the context is external or created within the API.
		/// </summary>
		private readonly bool isExternal;
		#endregion

		#region Properties
		/// <summary>
		/// Gets the alias repository.
		/// </summary>
		public Repositories.AliasRepository Aliases { get; private set; }

		/// <summary>
		/// Gets the author repository.
		/// </summary>
		public Repositories.AuthorRepository Authors { get; private set; }

		/// <summary>
		/// Gets the block repository.
		/// </summary>
		public Repositories.BlockRepository Blocks { get; private set; }

		/// <summary>
		/// Gets the category repository.
		/// </summary>
		public Repositories.CategoryRepository Categories { get; private set; }

		/// <summary>
		/// Gets the comment repository.
		/// </summary>
		public Repositories.CommentRepository Comments { get; private set; }

		/// <summary>
		/// Gets the media repository.
		/// </summary>
		public Repositories.MediaRepository Media { get; private set; }

		/// <summary>
		/// Gets the page repository.
		/// </summary>
		public Repositories.PageRepository Pages { get; private set; }

		/// <summary>
		/// Gets the page type repository.
		/// </summary>
		public Repositories.PageTypeRepository PageTypes { get; private set; }

		/// <summary>
		/// Gets the param repository.
		/// </summary>
		public Repositories.ParamRepository Params { get; private set; }

		/// <summary>
		/// Gets the post repository.
		/// </summary>
		public Repositories.PostRepository Posts { get; private set; }

		/// <summary>
		/// Gets the post type repository.
		/// </summary>
		public Repositories.PostTypeRepository PostTypes { get; private set; }

		/// <summary>
		/// Gets the ratings repository
		/// </summary>
		public Repositories.RatingRepository Ratings { get; private set; }
		#endregion

		/// <summary>
		/// Default constructor.
		/// </summary>
		public Api() : this(null) { }

		/// <summary>
		/// Creates a new API on an already open session.
		/// </summary>
		/// <param name="session">The session</param>
		internal Api(Data.IDbSession session) {
			this.DbSession = session ?? App.Store.OpenSession();
			isExternal = session != null;

			Aliases = new Repositories.AliasRepository(this.DbSession);
			Authors = new Repositories.AuthorRepository(this.DbSession);
			Blocks = new Repositories.BlockRepository(this.DbSession);
			Categories = new Repositories.CategoryRepository(this.DbSession);
			Comments = new Repositories.CommentRepository(this.DbSession);
			Media = new Repositories.MediaRepository(this.DbSession);
			Pages = new Repositories.PageRepository(this.DbSession);
			PageTypes = new Repositories.PageTypeRepository(this.DbSession);
			Params = new Repositories.ParamRepository(this.DbSession);
			Posts = new Repositories.PostRepository(this.DbSession);
			PostTypes = new Repositories.PostTypeRepository(this.DbSession);
			Ratings = new Repositories.RatingRepository(this.DbSession);
		}

		/// <summary>
		/// Saves the changes made to the api.
		/// </summary>
		/// <returns>The number of saved rows</returns>
		public void SaveChanges() {
			DbSession.SaveChanges();
		}

		/// <summary>
		/// Disposes the current api.
		/// </summary>
		public void Dispose() {
			if (!isExternal)
				DbSession.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}