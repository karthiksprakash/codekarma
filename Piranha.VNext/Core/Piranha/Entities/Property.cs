﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Piranha.Entities.Base;

namespace Piranha.Entities
{
    public sealed class Property : Model, Data.IModel, Data.IChanges
    {

        #region Properties

        /// <summary>
        /// Gets/sets the unique id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets/sets the id of the parent this property is attached to.
        /// </summary>
        public Guid ParentId { get; set; }

        /// <summary>
        /// Gets/sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/sets the value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets/sets the CLR type of the value.
        /// </summary>
        public string CLRType { get; set; }

        /// <summary>
        /// Gets/sets if this part should support multiple values.
        /// </summary>
        public bool IsCollection { get; set; }

        /// <summary>
        /// Gets/sets the current sort order.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Gets/sets when the model was initially created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets/sets when the model was last updated.
        /// </summary>
        public DateTime Updated { get; set; }
        
        #endregion 

        
        protected override FluentValidation.Results.ValidationResult Validate()
        {
            var validator = new PropertyValidator();
            return validator.Validate(this);
        }

        #region Validator
        private class PropertyValidator : AbstractValidator<Property>
        {
            public PropertyValidator()
            {
                RuleFor(m => m.Name).NotEmpty();
                RuleFor(m => m.Name).Length(0, 128);
                RuleFor(m => m.Value).NotEmpty();
                RuleFor(m => m.CLRType).NotEmpty();
                RuleFor(m => m.CLRType).Length(0, 512);
            }
        }
        #endregion
    }
}
