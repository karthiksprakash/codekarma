﻿/*
 * Copyright (c) 2014-2015 Håkan Edling
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 * 
 * http://github.com/piranhacms/piranha.vnext
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Piranha.Manager.Models.Page
{
	/// <summary>
	/// View model for the post list.
	/// </summary>
	public class ListModel
	{
		#region Inner classes
		/// <summary>
		/// An item in the post list.
		/// </summary>
		public class PageListItem
		{
			/// <summary>
			/// Gets/sets the unique id.
			/// </summary>
			public Guid Id { get; set; }

			/// <summary>
			/// Gets/sets the title.
			/// </summary>
			public string Title { get; set; }

			/// <summary>
			/// Gets/sets the name of the page type.
			/// </summary>
			public string Type { get; set; }

			/// <summary>
			/// Gets/sets the date the page was initially created.
			/// </summary>
			public DateTime Created { get; set; }

			/// <summary>
			/// Gets/sets the date the page was last updated.
			/// </summary>
			public DateTime Updated { get; set; }

			/// <summary>
			/// Gets/sets the published date.
			/// </summary>
			public DateTime? Published { get; set; }
		}

		/// <summary>
		/// An item in the page type list.
		/// </summary>
		public class PageTypeListItem
		{
			/// <summary>
			/// Gets/sets the display name.
			/// </summary>
			public string Name { get; set; }

			/// <summary>
			/// Gets/sets the unique slug.
			/// </summary>
			public string Slug { get; set; }

			/// <summary>
			/// Gets/sets if this is the currently selected page type.
			/// </summary>
			public bool IsActive { get; set; }
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets/sets the available items.
		/// </summary>
		public IList<PageListItem> Items { get; set; }

		/// <summary>
		/// Gets/sets the available page types.
		/// </summary>
		public IList<PageTypeListItem> PageTypes { get; set; }

		/// <summary>
		/// Gets/sets if no specific page type has been selected.
		/// </summary>
		public bool IsDefaultSelection { get; set; }
		#endregion

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ListModel() {
            Items = new List<PageListItem>();
            PageTypes = new List<PageTypeListItem>();
			IsDefaultSelection = true;
		}

		/// <summary>
		/// Gets the page list model.
		/// </summary>
		/// <param name="slug">Optional page type slug</param>
		/// <returns>The model</returns>
		public static ListModel Get(string slug = null) {
			using (var api = new Api()) {
				var m = new ListModel();

				if (!String.IsNullOrWhiteSpace(slug)) {
					m.Items = api.Pages.Get(where: p => p.Type.Slug == slug).Select(p => new PageListItem() {
						Id = p.Id,
						Title = p.Title,
						Type = p.Type.Name,
						Created = p.Created,
						Updated = p.Updated,
						Published = p.Published
					}).ToList();
					m.IsDefaultSelection = false;
				} else {
                    m.Items = api.Pages.Get().Select(p => new PageListItem()
                    {
						Id = p.Id,
						Title = p.Title,
						Type = p.Type.Name,
						Created = p.Created,
						Updated = p.Updated,
						Published = p.Published
					}).ToList();
				}
				m.PageTypes = api.PageTypes.Get().Select(t => new PageTypeListItem() {
					Name = t.Name,
					Slug = t.Slug,
					IsActive = t.Slug == slug
				}).ToList();
				return m;
			}
		}
	}
}