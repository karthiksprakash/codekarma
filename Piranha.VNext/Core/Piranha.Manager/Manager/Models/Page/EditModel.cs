﻿/*
 * Copyright (c) 2014-2015 Håkan Edling
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 * 
 * http://github.com/piranhacms/piranha.vnext
 * 
 */

using AutoMapper;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Piranha.Entities;

namespace Piranha.Manager.Models.Page
{
	/// <summary>
	/// View model for the page edit view.
	/// </summary>
	public class EditModel
	{
		#region Inner classes
		
		public enum SubmitAction
		{
			Save,
			Publish,
			Unpublish
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets/sets the unique id.
		/// </summary>
		public Guid? Id { get; set; }

		/// <summary>
		/// Gets/sets the id of the page type.
		/// </summary>
		public Guid TypeId { get; set; }

		/// <summary>
		/// Gets/sets the name of the page type.
		/// </summary>
		public string TypeName { get; set; }

        /// <summary>
        /// Gets/sets the sort order of the page.
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// Gets/sets if the page should be hidden from navigations.
        /// </summary>
        public bool IsHidden { get; set; }

		/// <summary>
		/// Gets/sets the author responsible for this content.
		/// </summary>
		public Guid AuthorId { get; set; }

		/// <summary>
		/// Gets/sets the title.
		/// </summary>
		[Required, StringLength(128)]
		public string Title { get; set; }

        /// <summary>
        /// Gets/sets the alternate navigation title.
        /// </summary>
        public string NavigationTitle { get; set; }

		/// <summary>
		/// Gets/sets the unique slug.
		/// </summary>
		[StringLength(128)]
		public string Slug { get; set; }

		/// <summary>
		/// Gets/sets the optional keywords.
		/// </summary>
		[StringLength(128)]
		public string Keywords { get; set; }

		/// <summary>
		/// Gets/sets the optional description.
		/// </summary>
		[StringLength(255)]
		public string Description { get; set; }

		/// <summary>
		/// Gets/sets the optional excerpt.
		/// </summary>
		[StringLength(512)]
		public string Excerpt { get; set; }

		/// <summary>
		/// Gets/sets the main page body.
		/// </summary>
		public string Body { get; set; }

		/// <summary>
		/// Gets/sets the optional route that should handle requests.
		/// </summary>
		[StringLength(255)]
		public string Route { get; set; }

		/// <summary>
		/// Gets/sets the optional view that should render requests.
		/// </summary>
		[StringLength(255)]
		public string View { get; set; }

		/// <summary>
		/// Gets/sets the publish date.
		/// </summary>
		public DateTime? Published { get; set; }
	
		/// <summary>
		/// Gets/sets the available authors.
		/// </summary>
		public SelectList Authors { get; set; }

        /// <summary>
        /// Gets/sets the available properties.
        /// </summary>
        [UIHint("PropertyList")]
        public IList<Property.EditModel> Properties { get; set; }

		/// <summary>
		/// Gets/sets the selected submit action.
		/// </summary>
		public SubmitAction Action { get; set; }
		#endregion

		/// <summary>
		/// Default constructor.
		/// </summary>
		public EditModel() {
			using (var api = new Api()) {
				Init(api);
			}
		}

		/// <summary>
		/// Creates a new edit model using the given api.
		/// </summary>
		/// <param name="api">The current api.</param>
		/// <param name="pagetype">The selected page type slug</param>
		public EditModel(Api api, string pagetype) {
			Init(api, pagetype);
            SortOrder = 1;
        }

		/// <summary>
		/// Gets the edit model for the page with the given id.
		/// </summary>
		/// <param name="api">The current api</param>
		/// <param name="id">The unique id</param>
		/// <returns>The model</returns>
		public static EditModel GetById(Api api, Guid id) {
			var page = api.Pages.GetSingle(id);

			if (page != null) {
				var m = Mapper.Map<Piranha.Entities.Page, EditModel>(page);
				m.TypeName = page.Type.Name;								
				return m;
			}
			return null;
		}

		/// <summary>
		/// Saves the current page edit model.
		/// </summary>
		/// <param name="api">The current api</param>
		public void Save(Api api) {
			var newModel = false;

			// Get or create page
			var page = Id.HasValue ? api.Pages.GetSingle(Id.Value) : null;
			if (page == null) {
				page = new Piranha.Entities.Page();
				newModel = true;
			}

			// Map values
			Mapper.Map<EditModel, Piranha.Entities.Page>(this, page);
		    if (!newModel)
		    {
		        page.Properties.ToList().ForEach( p=>
		        {
		            p.Value = Properties.FirstOrDefault(pe => pe.Id == p.Id).Value;
		        });
		    }

			// Check action
			if (Action == SubmitAction.Publish)
				page.Published = DateTime.Now;
			else if (Action == SubmitAction.Unpublish)
				page.Published = null;
			
			if (newModel)
				api.Pages.Add(page);
			api.SaveChanges();

			this.Id = page.Id;
		}

		#region Private methods
		/// <summary>
		/// Initializes the model.
		/// </summary>
		/// <param name="api">The current api</param>
		/// <param name="pagetype">The optional page type</param>
		private void Init(Api api, string pagetype = "") {
			// Get the page type
			if (!String.IsNullOrWhiteSpace(pagetype)) {
				var type = api.PageTypes.GetSingle(where: t => t.Slug == pagetype);

				TypeId = type.Id;
				TypeName = type.Name;
                Properties = new List<Property.EditModel>();
                type.Properties.ForEach(pr => Properties.Add(new Property.EditModel()
                {
                    Name = pr.Name,
                    CLRType = pr.CLRType,
                    IsCollection = pr.IsCollection,
                    InternalId = pr.InternalId,
                }));
			}

			// Get available authors
			var authors = api.Authors.Get();
			Authors = new SelectList(authors, "Id", "Name");
            		

			// Get default author
			var userid = App.Security.GetUserId();
			if (!String.IsNullOrEmpty(userid)) {
				// TODO
			}			
		}
		#endregion
	}
}