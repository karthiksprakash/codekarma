﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Piranha.Manager.Models.Property
{
    public class EditModel
    {
        /// <summary>
        /// Gets/sets the unique id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Get/sets the internal id.
        /// </summary>
        public string InternalId { get; set; }

        /// <summary>
        /// Gets/sets the id of the parent this property is attached to.
        /// </summary>
        public Guid ParentId { get; set; }

        /// <summary>
        /// Gets/sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/sets the value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets/sets the CLR type of the value.
        /// </summary>
        public string CLRType { get; set; }

        /// <summary>
        /// Gets/sets the current sort order.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Gets/sets if this part should support multiple values.
        /// </summary>
        public bool IsCollection { get; set; }

        /// <summary>
        /// Gets/sets when the model was initially created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets/sets when the model was last updated.
        /// </summary>
        public DateTime Updated { get; set; }
    }
}