﻿/*
 * Copyright (c) 2014-2015 Håkan Edling
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 * 
 * http://github.com/piranhacms/piranha.vnext
 * 
 */

using System;
using System.Linq;
using System.Web.Mvc;
using Piranha.Manager.Models.Page;

namespace Piranha.Areas.Manager.Controllers
{
	/// <summary>
	/// Controller for managing pages.
	/// </summary>
	[Authorize]
	[RouteArea("Manager", AreaPrefix="manager")]
    public class PageMgrController : ManagerController
    {
		/// <summary>
		/// Gets a list of the currently available pages
		/// </summary>
		/// <returns></returns>
		[Route("pages/{slug?}")]
		public ActionResult List(string slug = null) {
			return View(ListModel.Get(slug));
		}

		[Route("page/add/{type}")]
		public ActionResult Add(string type) {
			var model = new EditModel(api, type);

			ViewBag.Title = String.Format(Piranha.Manager.Resources.Page.AddTitle, model.TypeName);
			return View("Edit", model);
		}

		/// <summary>
		/// Gets the edit view for a new or existing page.
		/// </summary>
		/// <param name="id">The optional id</param>
		/// <returns>The view result</returns>
		[Route("page/edit/{id:Guid}")]
		public ActionResult Edit(Guid id) {
			var model = EditModel.GetById(api, id);

			ViewBag.Title = Piranha.Manager.Resources.Page.EditTitle;
			ViewBag.SubTitle = model.Published.HasValue ? 
				((model.Published.Value > DateTime.Now ? "Schedueled: " : "Published: ") + model.Published.Value.ToString("yyyy-MM-dd")) : "Unpublished";

			return View(model);
		}

		/// <summary>
		/// Saves the given page model.
		/// </summary>
		/// <param name="model">The model</param>
		/// <returns>The redirect result</returns>
		[HttpPost]
		[Route("page/save")]
		[ValidateInput(false)]
		[ValidateAntiForgeryToken]
		public ActionResult Save(EditModel model) {
			if (ModelState.IsValid) {
				model.Save(api);
				IsSaved = true;
				return RedirectToAction("Edit", new { id = model.Id });
			}
			if (model.Id.HasValue)
				ViewBag.Title = Piranha.Manager.Resources.Page.EditTitle;
			else ViewBag.Title = String.Format(Piranha.Manager.Resources.Page.AddTitle, model.TypeName);

			return View("Edit", model);
		}

		/// <summary>
		/// Deletes the page with the given id.
		/// </summary>
		/// <param name="id">The unique id</param>
		/// <returns>The redirect result</returns>
		[Route("page/delete/{id:Guid}")]
		public ActionResult Delete(Guid id) {
			api.Pages.Remove(id);
			api.SaveChanges();

			return RedirectToAction("List");
		}
	}
}