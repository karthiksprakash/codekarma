﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeKarma.Entities;
using Piranha.EntityFramework;

namespace CodeKarma.Dal
{
   class Db : BaseContext<Db>
   {

      public DbSet<ContactRequest> Type { get; set; }

      /// <summary>
		/// Default constructor.
		/// </summary>
		public Db() : base("codekarma", new MigrateDatabaseToLatestVersion<Db, Migrations.Configuration>()) { }

      /// <summary>
		/// Configures the data model.
		/// </summary>
		/// <param name="modelBuilder">The current model builder</param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
      {
         // ContactRequest
         modelBuilder.Entity<ContactRequest>().ToTable("ContactRequests");
         modelBuilder.Entity<ContactRequest>().Property(a => a.Name).HasMaxLength(255).IsRequired();
         modelBuilder.Entity<ContactRequest>().Property(a => a.Email).HasMaxLength(255).IsRequired();
         modelBuilder.Entity<ContactRequest>().Property(a => a.IpAddress).HasMaxLength(128);
         modelBuilder.Entity<ContactRequest>().Property(a => a.Message).HasMaxLength(2048);

         ////Contact
         //modelBuilder.Entity<Contact>().ToTable("Contacts");
         //modelBuilder.Entity<Contact>().Property(a => a.Name).HasMaxLength(255).IsRequired();
         //modelBuilder.Entity<Contact>().Property(a => a.Email).HasMaxLength(255).IsRequired();

         base.OnModelCreating(modelBuilder);
      }
   }
}
