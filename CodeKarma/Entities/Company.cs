﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Piranha.Data;
using Piranha.Entities;

namespace CodeKarma.Entities
{
   public class Company : Model, IModel, IChanges
   {
      /// <summary>
		/// Gets/sets the unique id.
		/// </summary>
		public Guid Id { get; set; }

      /// <summary>
		/// Gets/sets the Account number
		/// </summary>
		public int AccountNumber { get; set; }

      /// <summary>
		/// Gets/sets the Name
		/// </summary>
		public string Name { get; set; }

      public string Description { get; set; }

      public string Industry { get; set; }

      /// <summary>
		/// Gets/sets the Email
		/// </summary>
		public string Email { get; set; }

      /// <summary>
		/// Gets/sets the Website
		/// </summary>
		public string Website { get; set; }

      /// <summary>
      /// Gets/sets when the model was initially created.
      /// </summary>
      public DateTime Created { get; set; }

      /// <summary>
      /// Gets/sets when the model was last updated.
      /// </summary>
      public DateTime Updated { get; set; }

      public virtual ICollection<Contact> Contacts { get; set; }
      public virtual ICollection<Address> Addresses { get; set; }

      /// <summary>
		/// Method to validate model
		/// </summary>
		/// <returns>Returns the result of validation</returns>
		protected override FluentValidation.Results.ValidationResult Validate()
      {
         var validator = new CompanyValidator();
         return validator.Validate(this);
      }

      #region Validator
      private class CompanyValidator : AbstractValidator<Company>
      {
         public CompanyValidator()
         {
            RuleFor(m => m.AccountNumber).NotEmpty();
            RuleFor(m => m.Name).NotEmpty();
            RuleFor(m => m.Name).Length(0, 128);
            RuleFor(m => m.Email).NotEmpty();
            RuleFor(m => m.Email).Length(0, 128);
            RuleFor(m => m.Addresses).NotNull();
            RuleFor(m => m.Contacts).NotNull();
         }
      }
      #endregion
   }
}

