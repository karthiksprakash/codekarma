﻿/*
 * Copyright (c) 2014-2015 Håkan Edling
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 * 
 * http://github.com/piranhacms/piranha.vnext
 * 
 */

using System;
using FluentValidation;
using Piranha.Data;
using Piranha.Entities;

namespace CodeKarma.Entities
{
	/// <summary>
	/// Authors are used to sign content.
	/// </summary>
	public class Contact : Model, IModel, IChanges
	{
		#region Properties
		/// <summary>
		/// Gets/sets the unique id.
		/// </summary>
		public Guid Id { get; set; }

	   /// <summary>
	   /// Gets/sets the full name.
	   /// </summary>
	   public string Name => FirstName + " " + LastName;

      /// <summary>
      /// Gets/sets the FirstName
      /// </summary>
      public string FirstName { get; set; }

      /// <summary>
		/// Gets/sets the LastName
		/// </summary>
		public string LastName { get; set; }

      /// <summary>
      /// Gets/sets the email address.
      /// </summary>
      public string Email { get; set; }

      /// <summary>
		/// Gets/sets the Phone number.
		/// </summary>
		public string Phone { get; set; }

      /// <summary>
      /// Gets/sets the Title
      /// </summary>
      public string Title { get; set; }

      /// <summary>
      /// Gets/sets when the model was initially created.
      /// </summary>
      public DateTime Created { get; set; }

		/// <summary>
		/// Gets/sets when the model was last updated.
		/// </summary>
		public DateTime Updated { get; set; }

      /// <summary>
      /// Gets/sets the Company
      /// </summary>
      public virtual Company Company { get; set; }

      #endregion

      /// <summary>
      /// Method to validate model
      /// </summary>
      /// <returns>Returns the result of validation</returns>
      protected override FluentValidation.Results.ValidationResult Validate()
		{
			var validator = new ContactValidator();
			return validator.Validate(this);
		}

		#region Validator
		private class ContactValidator : AbstractValidator<Contact>
		{
			public ContactValidator()
			{
				RuleFor(m => m.FirstName).NotEmpty();
				RuleFor(m => m.FirstName).Length(0, 128);
            RuleFor(m => m.LastName).NotEmpty();
            RuleFor(m => m.LastName).Length(0, 128);
            RuleFor(m => m.Email).NotEmpty();
            RuleFor(m => m.Email).Length(0,128);
            RuleFor(m => m.Phone).NotEmpty();
            RuleFor(m => m.Phone).Length(0, 14);
         }
		}
		#endregion
	}
}
