﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Piranha.Data;
using Piranha.Entities;

namespace CodeKarma.Entities
{
   public class Address : Model, IModel, IChanges
   {
      /// <summary>
		/// Gets/sets the unique id.
		/// </summary>
		public Guid Id { get; set; }
      
      public int AddressType { get; set; }

      public string Name { get; set; }

      public string Phone1 { get; set; }
      public string Phone2 { get; set; }

      public string Fax { get; set; }

      public string PostOfficeBox { get; set; }

      public string Street1 { get; set; }
      public string Street2 { get; set; }
      public string Street3 { get; set; }
      public string City { get; set; }

      public string County { get; set; }

      public string State { get; set; }

      public string Country { get; set; }

      public virtual Company Company { get; set; }

      public virtual Contact PrimaryContact { get; set; }

      /// <summary>
      /// Gets/sets when the model was initially created.
      /// </summary>
      public DateTime Created { get; set; }

      /// <summary>
      /// Gets/sets when the model was last updated.
      /// </summary>
      public DateTime Updated { get; set; }

      /// <summary>
      /// Method to validate model
      /// </summary>
      /// <returns>Returns the result of validation</returns>
      protected override FluentValidation.Results.ValidationResult Validate()
      {
         var validator = new AddressValidator();
         return validator.Validate(this);
      }

      #region Validator
      private class AddressValidator : AbstractValidator<Address>
      {
         public AddressValidator()
         {
            RuleFor(m => m.Name).Length(0, 128);
            RuleFor(m => m.Phone1).NotEmpty();
            RuleFor(m => m.Phone1).Length(0, 14);
            RuleFor(m => m.Phone2).Length(0, 14);
            RuleFor(m => m.City).NotEmpty();
            RuleFor(m => m.City).Length(0, 128);
            RuleFor(m => m.State).NotEmpty();
            RuleFor(m => m.State).Length(0, 128);
            RuleFor(m => m.Country).NotEmpty();
            RuleFor(m => m.Country).Length(0, 128);
            RuleFor(m => m.Company).NotNull();
            RuleFor(m => m.PrimaryContact).NotNull();
         }
      }
      #endregion
   }
}
