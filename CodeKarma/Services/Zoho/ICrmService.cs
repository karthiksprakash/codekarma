using System.Threading.Tasks;
using CodeKarma.Entities;
using Piranha.Client.Models;

namespace CodeKarma.Services.Zoho
{
   public interface ICrmService
   {
        Task<ResponseModel<string>> AddNewContactRequestAsLeadAsync(string leadSource, ContactRequest contactRequest, bool isApproval = true);
   }
}