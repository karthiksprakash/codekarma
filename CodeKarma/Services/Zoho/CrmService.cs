﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using CodeKarma.Entities;
using Piranha.Client.Models;
using Piranha.Log;

namespace CodeKarma.Services.Zoho
{
   public class CrmService : ICrmService
   {
      private readonly string _zohoBaseUrl;
      private string _emailId;
      private string _password;
      private readonly string _authToken;

      public CrmService(string zohoBaseUrl, string emailId, string password)
      {
         _zohoBaseUrl = zohoBaseUrl;
         _emailId = emailId;
         _password = password;
      }

      public CrmService(ILog logger, string zohoBaseUrl, string authToken)
      {
         _zohoBaseUrl = zohoBaseUrl;
         _authToken = authToken;
      }

      public async Task<ResponseModel<string>> AddNewContactRequestAsLeadAsync(string leadSource, ContactRequest contactRequest, bool isApproval = true)
      {
         //Build the xml
         XElement leadXmlElement = new XElement("Leads", 
            new XElement("row", 
               new XElement("FL", leadSource, new XAttribute("val", "Lead Source")),
               new XElement("FL", "NA", new XAttribute("val", "Company")),
               new XElement("FL", contactRequest.Name, new XAttribute("val", "First Name")),
               new XElement("FL", "NA", new XAttribute("val", "Last Name")),
               new XElement("FL", contactRequest.Email, new XAttribute("val", "Email")),
               new XElement("FL", string.Format("{0}\nIPAddress:{1}", contactRequest.Message, contactRequest.IpAddress), new XAttribute("val", "Description")),
               new XAttribute("no", "1")
            ));

         //Build the URL
         string zohoPostUrl =
            string.Format("{0}Leads/insertRecords?authtoken={1}&scope=crmapi&xmlData={2}&isApproval={3}&duplicateCheck=2", _zohoBaseUrl,
               _authToken, leadXmlElement.ToString(SaveOptions.DisableFormatting), isApproval.ToString().ToLower());

         //Post the request
         HttpClient httpClient = new HttpClient();
         HttpResponseMessage response = await httpClient.PostAsync(zohoPostUrl, new StringContent(string.Empty));
          if (!response.IsSuccessStatusCode)
          {
              string responseContent = await response.Content.ReadAsStringAsync();
              return new ResponseModel<string>()
              {
                  Message = "Error adding new contact request to CRM. " + responseContent
              };
          }
          else
              return new ResponseModel<string>() {IsSuccess = true, Message = "Successfully added new contact request"};

      }
   }
}
